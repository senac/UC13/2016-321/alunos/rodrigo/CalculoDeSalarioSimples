/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculodesalario;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeSalarioTest {
 @Test
    public void deveCalcularSalarioDesenvolvedorAbaixoLimite() {
        Funcionario f = new Funcionario("jose", 1000, Cargo.DESENVOLVEDOR);
        CalculadoraDeSalario calculadoraDeSalario = new CalculadoraDeSalario();
        double salarioCalculado = calculadoraDeSalario.calcular(f);
        Assert.assertEquals(1000 * 0.9, salarioCalculado, 0.01);

    }

    @Test
    public void deveCalcularSalarioDesenvolvedorAcimaLimite() {
        CalculadoraDeSalario calculadoraDeSalario = new CalculadoraDeSalario();
        Funcionario funcionario = new Funcionario("Jonas", 5000, Cargo.DESENVOLVEDOR);
        double salarioCalculado = calculadoraDeSalario.calcular(funcionario);
        Assert.assertEquals(5000 * 0.80, salarioCalculado, 0.001);
    }

    @Test
    public void deveCalcularSalarioDBAETestadorAbaixoLimite() {
        CalculadoraDeSalario calculadoraDeSalario = new CalculadoraDeSalario();
        Funcionario funcionario = new Funcionario("Jonas", 1500, Cargo.DBA);
        double salarioCalculado = calculadoraDeSalario.calcular(funcionario);
        Assert.assertEquals(1500 * 0.85, salarioCalculado, 0.001);
    }

    @Test
    public void deveCalcularSalarioDBAETestadorAcimDoLimite() {
        CalculadoraDeSalario calculadoraDeSalario = new CalculadoraDeSalario();
        Funcionario funcionario = new Funcionario("Jonas", 3000, Cargo.DBA);
        double salarioCalculado = calculadoraDeSalario.calcular(funcionario);
        Assert.assertEquals(3000 * 0.75, salarioCalculado, 0.001);
    }

}

