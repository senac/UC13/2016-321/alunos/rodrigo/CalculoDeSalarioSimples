/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculodesalario;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeSalario {

    public double calcular(Funcionario funcionario) {

        if (funcionario.getCargo().equals(Cargo.DESENVOLVEDOR)) {

            if (funcionario.getSalario() > 3000) {
                return 5000 * 0.8;
            } else {
                return 1000 * 0.9;
            }
        }

        if (funcionario.getSalario() > 2500) {
            return 3000 * 0.75;
        }

        return 1500 * 0.85;

    }

}
